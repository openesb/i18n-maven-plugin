package net.openesb.maven.plugin.i18n;

import java.io.File;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

/**
 * 
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public abstract class I18nAbstractMojo extends AbstractMojo {
    
    /**
     * The directory for compiled classes.
     */
    @Parameter( defaultValue = "${project.build.outputDirectory}", required = true, readonly = true )
    private File outputDirectory;
    
    @Component
    protected MavenProject project;

    public File getOutputDirectory() {
	return outputDirectory;
    }

    public MavenProject getProject() {
	return project;
    }
}
