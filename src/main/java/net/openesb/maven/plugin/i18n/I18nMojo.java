package net.openesb.maven.plugin.i18n;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

/**
 * Extracts internationalizable text from class files and puts these strings in a 
 * resource bundle.
 * 
 * The directory of classfiles needs to be specified as well as the location of the 
 * resource bundle to be updated.
 * 
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Mojo( name = "i18n", defaultPhase = LifecyclePhase.PROCESS_CLASSES, requiresDependencyResolution = ResolutionScope.RUNTIME, threadSafe = true )
public class I18nMojo extends I18nAbstractMojo {
    
    @Parameter( defaultValue = "")
    private String prefix;
    
    @Parameter(required = true)
    private String pattern;
	
    @Parameter(defaultValue = "true")
    private boolean strict = true;
    
    @Parameter
    private String prefixU;
    
    @Parameter(alias = "dir", defaultValue = "${project.build.outputDirectory}")
    private File classesDir;
    
    @Parameter
    private File bundle;
    
    private Pattern splitter;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
	if (classesDir == null) {
            throw new MojoFailureException("Directory must be specified using the " +
                "\"dir\" attribute");
        }
        if (bundle == null) {
            throw new MojoFailureException("File must be specified");
        }
	
        splitter = Pattern.compile(pattern, Pattern.DOTALL);
        extract(bundle.getAbsolutePath());
    }

    /*
     * Converts unicodes to encoded &#92;uxxxx and escapes
     * special characters with a preceding slash
     * From JDK Properties
     */
    private String saveConvert(String theString, boolean escapeSpace) {
        int len = theString.length();
        int bufLen = len * 2;
        if (bufLen < 0) {
            bufLen = Integer.MAX_VALUE;
        }
        StringBuilder outBuffer = new StringBuilder(bufLen);

        for(int x=0; x<len; x++) {
            char aChar = theString.charAt(x);
            // Handle common case first, selecting largest block that
            // avoids the specials below
            if ((aChar > 61) && (aChar < 127)) {
                if (aChar == '\\') {
                    outBuffer.append('\\'); outBuffer.append('\\');
                    continue;
                }
                outBuffer.append(aChar);
                continue;
            }
            switch(aChar) {
            case ' ':
                if (x == 0 || escapeSpace) 
                    outBuffer.append('\\');
                outBuffer.append(' ');
                break;
            case '\t':outBuffer.append('\\'); outBuffer.append('t');
            break;
            case '\n':outBuffer.append('\\'); outBuffer.append('n');
            break;
            case '\r':outBuffer.append('\\'); outBuffer.append('r');
            break;
            case '\f':outBuffer.append('\\'); outBuffer.append('f');
            break;
            case '=': // Fall through
            case ':': // Fall through
            case '#': // Fall through
            case '!':
                outBuffer.append('\\'); outBuffer.append(aChar);
                break;
            default:
                if ((aChar < 0x0020) || (aChar > 0x007e)) {
                    outBuffer.append('\\');
                    outBuffer.append('u');
                    outBuffer.append(toHex((aChar >> 12) & 0xF));
                    outBuffer.append(toHex((aChar >>  8) & 0xF));
                    outBuffer.append(toHex((aChar >>  4) & 0xF));
                    outBuffer.append(toHex( aChar        & 0xF));
                } else {
                    outBuffer.append(aChar);
                }
            }
        }
        return outBuffer.toString();
    }

    /**
     * Convert a nibble to a hex character
     * @param    nibble  the nibble to convert.
     */
    private static char toHex(int nibble) {
        return hexDigit[(nibble & 0xF)];
    }

    /** A table of hex digits */
    private static final char[] hexDigit = {
        '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'
    };

    /**
     * Extracts internationalizable text out of a jar and puts it in a properties file
     * 
     * @param dir classes dir
     * @param propertiesPath properties to update
     */
    public void extract(String propertiesPath) throws MojoFailureException {
        try {
            boolean duplicateIDsError = false;
            boolean couldBeImproved = false;
            String effectiveprefix = this.prefixU != null ? this.prefixU.toUpperCase() : this.prefix;
            if (effectiveprefix == null) {
                effectiveprefix = "";
            }

            // Iterate over the directory/directories and gather bundle information 
            List<TextEntry> list = new ArrayList<TextEntry>();
         //   if (dirSets.size() <= 0) {
                readDir(list, classesDir.getAbsolutePath(), classesDir.getAbsolutePath(), true);
         /*   } else {
                Iterator<DirSet> it = dirSets.iterator();
                while (it.hasNext()) {
                    DirSet dirSet = it.next();
                    DirectoryScanner ds = null;
                    ds = dirSet.getDirectoryScanner(getProject());

                    File rootDir = dirSet.getDir(getProject());
                    String[] srcDirs = ds.getIncludedDirectories();
                    for (int jj = 0; jj < srcDirs.length; jj++) {
                        readDir(list, rootDir.getAbsolutePath(), 
                            rootDir.getAbsolutePath() + File.separator + srcDirs[jj],
                            false);
                    }
                }
            }
	    */

            // Prepare bundle in memory buffer
            StringWriter outbuf = new StringWriter();
            PrintWriter out = new PrintWriter(outbuf);

            out.println("# DO NOT EDIT");
            out.println("# THIS FILE IS GENERATED AUTOMATICALLY FROM JAVA SOURCES/CLASSES");
            out.println();

            // Sort by text
            TextEntry[] entries = list.toArray(new TextEntry[list.size()]);
            Set<TextEntry> sortedByText = new TreeSet<TextEntry>(new Comparator<TextEntry>() {
                public int compare(TextEntry lhs, TextEntry rhs) {
                    return lhs.getText().compareTo(rhs.getText());
                }
            });
            sortedByText.addAll(Arrays.asList(entries));

            // Build BY-ID and BY-CONTENT maps
            Map<String, List<TextEntry>> byId = new HashMap<String, List<TextEntry>>();
            Map<String, List<TextEntry>> byContent = new HashMap<String, List<TextEntry>>();
            for (int i = 0; i < entries.length; i++) {
                TextEntry c = entries[i];
                if (byId.get(c.getID()) == null) {
                    byId.put(c.getID(), new ArrayList<TextEntry>());
                }
                byId.get(c.getID()).add(c);

                if (byContent.get(c.getContent()) == null) {
                    byContent.put(c.getContent(), new ArrayList<TextEntry>());
                }
                byContent.get(c.getContent()).add(c);
            }

            // Print all messages to the bundle
            for (Iterator<TextEntry> iter = sortedByText.iterator(); iter.hasNext();) {
                TextEntry e = iter.next();
                List<TextEntry> sources = byId.get(e.getID());

                // Collect in which classes this message was used and check if there 
                // are duplicate IDs
                List<String> classnames = new ArrayList<String>();
                for (Iterator<TextEntry> iterator = sources.iterator(); iterator.hasNext();) {
                    TextEntry e2 = iterator.next();

                    // Sanitize classname: slashes -> dots, no .class at the end
                    String clname = e2.getClassname();
                    clname = clname.replace(File.separatorChar, '.');
                    clname = clname.replace('$', '.') + '\t';
                    clname = clname.replace(".class\t", "");
                    classnames.add(clname);

                    // Check for duplicates
                    if (!e2.getContent().equals(e.getContent())) {
                        duplicateIDsError = true;
                        System.err.println();
                        System.err.println("DIFFERENT TEXTS, SAME IDS: ");
                        System.err.println(e.getClassname());
                        System.err.println(e.getText());
                        System.err.println(e2.getClassname());
                        System.err.println(e2.getText());
                    }
                }

                // Write out classnames in which this message was used
                Collections.sort(classnames);
                for (String classname: classnames) {
                    out.println("# " + classname);
                }

                // Write the actual message
                out.println(effectiveprefix + e.getID() + " = " + saveConvert(e.getContent(), false));
                out.println();
            }

            // Check for duplicate texts with different IDs
            for (Iterator<Map.Entry<String, List<TextEntry>>> iter = byContent.entrySet().iterator(); iter.hasNext();) {
                Map.Entry<String, List<TextEntry>> e = (Map.Entry<String, List<TextEntry>>) iter.next();
                List<TextEntry> dups = (List<TextEntry>) e.getValue();
                Set<TextEntry> ids = new TreeSet<TextEntry>(new Comparator<TextEntry>() {
                    public int compare(TextEntry lhs, TextEntry rhs) {
                        return lhs.getID().compareTo(rhs.getID());
                    }
                });
                ids.addAll(dups);
                if (ids.size() > 1) {
                    System.err.println();
                    System.err.println("SAME TEXTS, DIFFERENT IDS");
                    for (Iterator<TextEntry> iterator = dups.iterator(); iterator.hasNext();) {
                        couldBeImproved = true;
                        TextEntry dup = iterator.next();
                        System.err.println(dup.getClassname());
                        System.err.println(dup.getText());
                    }
                }
            }

            // Close memory buffer (there will be no resource leak if this is not done)
            out.close();
            out = null;

            // Update bundle if contents has changed
            String old = read(propertiesPath);
            String newContents = outbuf.getBuffer().toString();

            //if we are on cygwin using unix Eol conventions...
            if (isUnixEol(old) && !isUnixEol(newContents)) {
                //convert new to same eol conventions as old:
                newContents = toUnixEols(newContents);

                //NOTE:  this assumes we DO NOT generate the initial file on cygwin.
            }

            if (!newContents.equals(old)) {
                System.out.println("Updating " + propertiesPath);
                write(propertiesPath, newContents);
            } else {
                System.out.println("Up to date: " + propertiesPath);
            }

            // Check results
            if (duplicateIDsError) {
                throw new MojoFailureException("Duplicate ids but different texts; "
                    + "see console output for details"); 
            }

            if (couldBeImproved && strict) {
                throw new MojoFailureException("Duplicate texts but different ids; "
                    + "see console output for details"); 
            }
        } catch (Exception e) {
            throw new MojoFailureException("Failed to extract bundle information or create the bundle. Bundle=" 
                + propertiesPath + ". Error was: " + e, e);
        } 
    }

    /**
     * @param s Stream to close
     */
    public static void safeClose(InputStream s) {
        if (s != null) {
            try {
                s.close();
            } catch (Exception ignore) {
                // ignore
            }
        }
    }

    /**
     * @param s Stream to close
     */
    public static void safeClose(OutputStream s) {
        if (s != null) {
            try {
                s.close();
            } catch (Exception ignore) {
                // ignore
            }
        }
    }

    /**
     * @param s Stream to close
     */
    public static void safeClose(Writer s) {
        if (s != null) {
            try {
                s.close();
            } catch (Exception ignore) {
                // ignore
            }
        }
    }

    private static String read(String path) throws Exception {
        File f = new File(path);
        if (!f.exists()) {
            return null;
        }
        int len = (int) f.length();
        byte[] buf = new byte[len];
        FileInputStream inp = null;
        try {
            inp = new FileInputStream(path);
            int nbread = inp.read(buf);
            if (nbread != len) {
                throw new IOException(nbread + " read, " + len + " length"); 
            }
            return new String(buf);
        } finally {
            safeClose(inp);
        }
    }

    private static void write(String path, String contents) throws Exception {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(path);
            out.write(contents.getBytes("8859_1"));
        } finally {
            safeClose(out);
        }
    }

    /**
     * TAG_NOTHING means that the ConstantPool Entry is invalid.
     **/
    public static final int TAG_NOTHING = -1;

    /**
     * TAG_UTF8 = CONSTANT_UTF8
     **/
    public static final int TAG_UTF8 = 1;

    /**
     * TAG_INTEGER = CONSTANT_INTEGER
     **/
    public static final int TAG_INTEGER = 3;

    /**
     * TAG_FLOAT = CONSTANT_FLOAT
     **/
    public static final int TAG_FLOAT = 4;

    /**
     * TAG_LONG = CONSTANT_LONG
     **/
    public static final int TAG_LONG = 5;

    /**
     * TAG_DOUBLE = CONSTANT_DOUBLE
     **/
    public static final int TAG_DOUBLE = 6;

    /**
     * TAG_CLASS = CONSTANT_CLASS
     **/
    public static final int TAG_CLASS = 7;

    /**
     * TAG_STRING = CONSTANT_STRING
     **/
    public static final int TAG_STRING = 8;

    /**
     * TAG_FIELDREF = CONSTANT_FIELDREF
     **/
    public static final int TAG_FIELDREF = 9;

    /**
     * TAG_METHODREF = CONSTANT_METHODREF
     **/
    public static final int TAG_METHODREF = 10;

    /**
     * TAG_INTERFACEREF = CONSTANT_INTERFACEREF
     **/
    public static final int TAG_INTERFACEREF = 11;

    /**
     * TAG_NAMETYPE  = CONSTANT_NAMETYPE
     **/
    public static final int TAG_NAMETYPE = 12;

    /**
     * A collection of string entries associated with a class in a URL
     * 
     * @author fkieviet
     */
    private class TextEntry {
        private String jarURL;
        private String classname;
        private String text;
        private String id;
        private String content;

        /**
         * @param jarurl jar
         * @param classname classname
         * @param text String
         */
        public TextEntry(String jarurl, String classname, String text) {
            jarURL = jarurl;
            this.classname = classname;
            this.text = text;
        }

        /**
         * Getter for classname
         *
         * @return String
         */
        public String getClassname() {
            return classname;
        }

        /**
         * Setter for classname
         *
         * @param classname StringThe classname to set.
         */
        public void setClassname(String classname) {
            this.classname = classname;
        }

        /**
         * Getter for jarURL
         *
         * @return String
         */
        public String getJarURL() {
            return jarURL;
        }

        /**
         * Getter for strings
         *
         * @return List<String>
         */
        public String getText() {
            return text;
        }

        /**
         * @return ID
         */
        public String getID() {
            if (id == null) {
                Matcher m = splitter.matcher(text);
                if (!m.matches()) {
                    throw new RuntimeException("String not match pattern: " + text);
                }
                id = m.group(1);
                content = m.group(3);
            }
            return id;
        }

        /**
         * @return content
         */
        public String getContent() {
            if (content == null) {
                getID();
            }
            return content;
        }

        /**
         * @param s to test
         * @return true if matches
         */
        public boolean matches(String s) {
            Matcher m = splitter.matcher(s);
            return m.matches();
        }
    }

    private static boolean isArchive(ZipEntry entry) {
        for (int i = 0; i < mArchiveExtensions.length; i++) {
            if (entry.getName().endsWith(mArchiveExtensions[i])) {
                return true;
            }
        }
        return false;
    }

    private static String toUnixEols(String xx)
    //delete CR chars
    {
        if (xx != null) {
            return xx.replaceAll("\r", "");
        }

        return xx;
    }

    private static boolean isUnixEol(String xx) 
    //true if string has no CR chars
    {
        if (xx == null) return false;

        return !(xx.contains("\r"));
    }

    private static String[] mArchiveExtensions = new String[] {".jar", ".zip", ".nbm", ".war", ".ear", ".rar", ".sar"};

    private void readDir(List<TextEntry> list, String root, String currentPath, boolean recurse) throws Exception {
        File[] entries = new File(currentPath).listFiles();
        for (int i = 0; i < entries.length; i++) {
            if (entries[i].isDirectory() && recurse) {
                readDir(list, root, entries[i].getAbsolutePath(), recurse);
            } else {
                if (entries[i].getName().endsWith(".class")) {
                    InputStream inp = null;

                    try {
                        inp = new FileInputStream(entries[i]);
                        List<String> strings = readStrings(inp);
                        for (Iterator<String> iter = strings.iterator(); iter.hasNext();) {
                            String s = iter.next();
                            Matcher m = splitter.matcher(s);
                            if (m.matches()) {
                                list.add(new TextEntry(currentPath, 
                                    entries[i].getAbsolutePath().substring(root.length() + 1), s));
                            }
                        }
                    } catch (Exception ex) {
                        throw new Exception("Inspection failed of " + entries[i].getName() + ": " + ex, ex);
                    } finally {
                        safeClose(inp);
                    }
                }
            }
        }
    }

    /**
     * @param list List<TextEntries>
     * @param jar jar to explore
     * @param currentPath Path prefix
     * @throws Exception on fault
     */
    public void readJar(List<TextEntry> list, InputStream jar, String currentPath) throws Exception {
        ZipInputStream inp = new ZipInputStream(jar);
        for (;;) {
            ZipEntry entry = inp.getNextEntry();
            if (entry == null) {
                break;
            } else if (entry.isDirectory()) {
                // Ignore
            } else if (isArchive(entry)) {
                String path = currentPath + entry.getName() + "#/";
                readJar(list, inp, path);
            } else {
                if (entry.getName().endsWith(".class")) {
                    try {
                        List<String> strings = readStrings(inp);
                        for (Iterator<String> iter = strings.iterator(); iter.hasNext();) {
                            String s = iter.next();
                            Matcher m = splitter.matcher(s);
                            if (m.matches()) {
                                list.add(new TextEntry(currentPath, entry.getName(), s));
                            }
                        }
                    } catch (Exception ex) {
                        throw new Exception("Inspection failed of " + entry.getName() + ": " + ex, ex);
                    } 
                }
            }
        }
    }

    /**
     * Reads all strings from the constant pool of a class file
     * 
     * @param s class file
     * @return list of strings
     * @throws Exception on failure
     */
    public static List<String> readStrings(InputStream s) throws Exception {
        DataInputStream inp = new DataInputStream(s);

        // Sentinel
        int magic = inp.readInt();
        if (magic != 0xCAFEBABE) {
            throw new Exception("Invalid Magic Number");
        }

        // Version
        // short minorVersion = 
        inp.readShort();
        // short majorVersion = 
        inp.readShort();

        // Constant pool
        int nEntries = inp.readShort();
        List<String> strings = new ArrayList<String>();
        for (int i = 1; i < nEntries; i++) {
            byte tagByte  = inp.readByte();

            switch(tagByte) {
            case TAG_UTF8 :
                String utfString = inp.readUTF();
                strings.add(utfString);
                break;
            case TAG_INTEGER:
                // int intValue = 
                inp.readInt();
                break;
            case TAG_FLOAT:
                // float floatValue = 
                inp.readFloat();
                break;
            case TAG_LONG:
                // long longValue  = 
                inp.readLong();
                // Long takes two ConstantPool Entries.
                i++;
                break;
            case TAG_DOUBLE:
                // double doubleValue = 
                inp.readDouble();
                // Double takes two ConstantPool Entries.
                i++;
                break;
            case TAG_CLASS: {
                // int classIndex  =  
                inp.readShort();
                break;
            }
            case TAG_STRING:
                // int stringIndex =  
                inp.readShort();
                // TODO
                break;
            case TAG_FIELDREF: {
                // int classIndex  = 
                inp.readShort();
                // int nameType = 
                inp.readShort();
                break;
            }
            case TAG_METHODREF: {
                // int classIndex  = 
                inp.readShort();
                // int nameType = 
                inp.readShort();
                break;
            }
            case TAG_INTERFACEREF: {
                // int classIndex  = 
                inp.readShort();
                // int nameType = 
                inp.readShort();
                break;
            }
            case TAG_NAMETYPE: {
                // int nameIndex = 
                inp.readShort();
                // int descIndex = 
                inp.readShort();
                break;
            }
            default:
                throw new Exception("Unknown tagbyte " + tagByte
                    + " in constant pool (entry " + i + " out of " + nEntries + ")");
            }
        }
        return strings;
    }
}
