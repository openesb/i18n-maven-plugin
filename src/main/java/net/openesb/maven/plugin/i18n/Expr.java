/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.openesb.maven.plugin.i18n;

/**
 * A nested XPath expr.
 */
public class Expr {
    private String query;
    private String i18nAttr;
    private String keyAttr;

    // Bean constructor
    public Expr() {
    }

    public String getQuery() {
	return query;
    }

    public void setQuery(String query) {
	this.query = query;
    }

    public String getI18nAttr() {
	return i18nAttr;
    }

    public void setI18nAttr(String attr) {
	this.i18nAttr = attr;
    }

    public String getKeyAttr() {
	return keyAttr;
    }

    public void setKeyAttr(String keyAttr) {
	this.keyAttr = keyAttr;
    }

    public String toString() {
	return "Expr[" + getQuery() + "]";
    }
    
}
