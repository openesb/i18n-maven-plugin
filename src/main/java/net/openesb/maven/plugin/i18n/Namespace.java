/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.openesb.maven.plugin.i18n;

/**
 * A nested 'namespace'.
 */
public class Namespace {
    private String mPrefix;
    private String mUri;

    // Bean constructor
    public Namespace() {
    }

    public String getPrefix() {
	return mPrefix;
    }

    public String getUri() {
	return mUri;
    }

    public void setPrefix(String prefix) {
	mPrefix = prefix;
    }

    public void setUri(String uri) {
	mUri = uri;
    }

    public String toString() {
	return "NS[" + getPrefix() + "=" + getUri() + "]";
    }
    
}
